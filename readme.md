# traj
Generator for a simulation of flying objects in 2D space and a REST API for simulation logs.

# "Quick"start
Clone and cd into the repository.

    $ git clone git@gitlab.com:UpDownUp/traj.git
    $ cd traj

Compile `traj.cpp` and `traj_api.cpp` into executables:

    $ g++ -std=c++17 src/traj.cpp     -o traj 
    $ g++ -std=c++17 src/traj_api.cpp -o traj_api

To check that everything works alright, run a quick mini-simulation with a few objects and a long update interval:

    $ ./traj -lvv -n 5 -d 1500

Then fetch the trajectory of the first object:

    $ ./traj_api --traj
    $ cat response.json

To set up the REST API, first create and enter a python virtual environment, and install pip requirements (at the time of writing, only flask):

    $ python venv .venv
    $ source .venv/bin/activate         # location may vary
    $ pip install -r requirements.txt

Run the flask server. Note that this will occupy the shell with server debug output. If you are limited to one shell, you can pass the server off to a background process with nohup instead of running it as a foreground process normally:

    $ python server.py          # OR
    $ nohup python server.py &  # Note: Requires GNU coreutils

Once the server is up, you can make a request to it using curl, for example:

    $ curl -X POST -H "Content-Type: application/json" -d '{"mode": "sector","id": "B"}' http://127.0.0.1:5000/api/traj > server_response.json
    $ cat server_response.json

You can also access http://127.0.0.1:5000/api/traj on your browser (which has no `index.html` at the time of writing) and use your browser's developer tools to interact with the API.

If everything works, you are ready to run a full (time-consuming) simulation.

## Full simulation

Rerun `traj`, using the proper values for arguments:

    $ ./traj -l -n 500 -d 0.15

On my computer, this took about 8 minutes and generated a 15.7 GB `simulation_log.txt`.

After the program has exited successfully and the full `simulation_log.txt` is created, you can query the API (or run `traj_api`) as before.

This will be much slower - on my computer, `./traj_api.exe --sector B 1699665000 1699666000` with a full `simulation_log.txt` took just under 5 minutes.

# Documentation

## Querying API
To query the API, make sure the server is running, then make a POST request with a header including `Content-Type: application/json` to `http://127.0.0.1:5000/api/traj`. 

The payload of the POST request is a JSON like this:

    {
        "mode":    "traj", 
        "id":      "00000000000000000000000000000003", 
        "start_ts":"1699650000", 
        "end_ts":  "1699670000"
    }

* `mode` accepts either `"traj"` or `"sector"`, indicating query #1 and query #2 respectively. Required.
* `id` is the sector or object ID. Required.
* * For a `sector` query, it is a one-character string such as `"C"`.
* `start_ts` and `end_ts` are start and end timestamps of the query. Optional, by default they are set to `1699635515` and `1699671515` respectively, the full time interval of the simulation.

### Response format

The server returns a JSON containing an array with the results. A `sector` query returns a simple array of update messages posted from that sector, and a `traj` query returns an array of a registration message and a trajectory sub-array containing the update messages of the queried object.

* Registration messages are JSON objects containing immutable data on the object, such as payload. Each object posts a registration message only once, when it is constructed by the simulator.
* Update messages are JSON objects containing mutable information on the object such as position and heading (angle), and an update message is posted by each object at every update (every 150 ms according to spec).

You should be able to pipe `server_response.json` (or whatever you choose to save the API response as) into a specialized JSON parser for visualization or analysis or whatever, provided it is configured to parse the same field names used in the response JSON.

Note that `simulation_log.txt` is effectively a list of JSON objects separated by newlines, but does not match JSON spec and cannot be parsed as JSON.

## `server.py`,  `traj` and `traj_api`
`server.py` is only for taking POST requests and piping them to `traj_api`, then responding to the API request with the `response.json` provided by `traj_api`. 

`traj_api` scans through `simulation_log.txt` according to its given arguments and writes a `response.json` of the result of the query. 

`traj` is responsible for running the simulation and creating the `simulation_log.txt`.

For more information on using `traj` or `traj_api` separately, run 

    ./traj --help   # or ./traj_api --help

To see different example usages.

# Current limitations

* Only one `simulation_log.txt` exists at a time, and is overwritten (not appended) at each run. This means that multiple simulations cannot be queried together.
* API queries on a full `simulation_log.txt` are rather slow at around 5-10 minutes.
* No straightforward GUI/web interface for the API.
* No visualizer for the world, either.
* Regex conformity of object IDs is not checked by `traj_api`. This is OK if using `traj` for simulation generation, as it generates object IDs as serial 32-character hexstrings which always match `/[0-9a-z]{32}/`.

# Acknowledgements
Thanks to Jarryd Beck and friends for [cxxopts](https://github.com/jarro2783/cxxopts) simplifying argument handling.
