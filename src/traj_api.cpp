#include <iostream>
#include <fstream>
#include <string>
#include <chrono>
using namespace std;

#include "cxxopts.hpp"  // cxxopts  3.1.1   MIT (https://github.com/jarro2783/cxxopts)

// -- File IO, Initialized at setup
ifstream input_log;
ofstream output_log;

// -- typedefs and global struct
typedef enum {
    API_CALL_TRAJ,
    API_CALL_SECTOR
} api_call_t;

struct {
    api_call_t call     = API_CALL_TRAJ;
    uint32_t start_ts   = 1699635515;
    uint32_t end_ts     = 1699671515;
    string object_id    = "00000000000000000000000000000000";
    char sector_id      = 'A';
} conf;

typedef enum {
    MSG_TYPE_CONF,
    MSG_TYPE_REG,
    MSG_TYPE_TRAJ
} msg_type_t;

// -- Parsing helper functions
msg_type_t identify_msg(string line) {
    // Only trajectory messages have ETA element
    if (line.find("\"eta\":") != string::npos) {
        return MSG_TYPE_TRAJ;
    // Only registration messages have a payload element.
    } else if (line.find("\"payload\":") != string::npos) {
        return  MSG_TYPE_REG;
    } else {
        return  MSG_TYPE_CONF;
    }
}

// just parsing each line into json multiplies runtime about 6-7fold.
// Manual JSON parsing is not as maintainable, but essential since minimizing runtime is a priority.
string extract_keyvalue(string line, string key) {
    string value_str = "";
    size_t keypos = line.find(key);

    if (keypos != string::npos) {
        // Assume delimiter is ','. If no ',' found after key,
        // then delimiter is closing curly bracket        
        string delimiter = ",";
        if (line.find(delimiter, keypos) == string::npos) {
            delimiter = "}";
        }

        value_str = line.substr(
                            line.find(key)          + key.length(),
                            line.find(delimiter)  - (line.find(key) + key.length())
                        );
    }
    return value_str;
}

// -- API calls
void api_object_traj() {
    string line;
    bool pastfirst = false;
    
    while (getline(input_log, line)) {
        double ts = 0;

        // Comb through registration messages for the one from our object
        if (identify_msg(line) == MSG_TYPE_REG) {
            if (line.find(conf.object_id) != string::npos) {
                output_log << endl << line << ",";
                output_log << endl << "{\"trajectory\":[";
            }
        } else if (identify_msg(line) == MSG_TYPE_TRAJ) {
            // MSG_TYPE_TRAJ messages are guaranteed to have a timestamp element.
            ts = stod(extract_keyvalue(line, "\"t\":"));
            if (ts >= conf.start_ts && ts <= conf.end_ts) {
                if (line.find(conf.object_id) != string::npos) {
                    output_log << (pastfirst ?  "," : "") << endl << line;
                    pastfirst = true;
                }
            }
        }
    }
    if (pastfirst) {
        // close up trajectory array when no more lines to write
        output_log << "]}" << endl;
    } else {
        cerr << "Nothing written - either object ID is not found or timestamps invalid." << endl;
        output_log << endl << "{\"Error\": \"No matches found - either object ID (" << conf.object_id <<
                              ") is not present or requested timestamps (start: " << conf.start_ts <<
                              ((conf.start_ts == 1699635515) ? " (unchanged from default)" : "") <<
                              ", end: " << conf.end_ts <<
                              ((conf.end_ts   == 1699671515) ? " (unchanged from default)" : "") <<
                              ") are outside simulation timeframe.\"}";
    }
}

void api_sector_history() {
    string line;
    double ts;
    bool pastfirst = false;
    
    while (getline(input_log, line)) {
        if (identify_msg(line) == MSG_TYPE_TRAJ) {
            // MSG_TYPE_TRAJ messages are guaranteed to have a timestamp element.
            ts = stod(extract_keyvalue(line, "\"t\":"));
            if (ts >= conf.start_ts && ts <= conf.end_ts) {
                //printf("%c, \n", extract_keyvalue(line, "\"sector\":")[1]);
                if (extract_keyvalue(line, "\"sector\":")[1] == conf.sector_id) {
                    output_log << (pastfirst ?  "," : "") << endl << line;
                    pastfirst = true;
                }
            }
        }
    }
    if (!pastfirst) {
        cerr << "No sector activity found in given timeframe." << endl;
        output_log << endl << "{\"Error\": \"No activity in the sector in the given timeframe (not actually an error).\"}";
    }
    // prettification? out of scope
}

// -- Opens files for I/O and handles arguments
uint8_t setup(int argc, char* argv[]) {

    cxxopts::Options options("traj_api.cpp", "Get flying objects' simulation data");
    options.add_options()
        ("traj",   "Get data and trajectory of object by ID")
        ("sector", "Get activity over a sector")
        ("id", "Object or sector ID", cxxopts::value<std::string>())
        ("s, start_ts", "Start timestamp", cxxopts::value<uint32_t>())
        ("e, end_ts", "End timestamp", cxxopts::value<uint32_t>())
        ("h,help", "Show info");

    cxxopts::ParseResult result;
    try {
        options.parse_positional({"id", "start_ts", "end_ts"});
        result = options.parse(argc, argv);
    } catch(cxxopts::exceptions::exception e) {
        cout << "Exception caught during argument parsing: " << e.what() << endl;
        output_log << "{\"Error\": \"Exception caught during argument parsing: " << e.what() << "\"}" << endl;
        return EXIT_FAILURE;
    }
     
    std::cout << "Parsed options:" << std::endl;
    for (const auto& kvp : result.arguments()) {
        std::cout << kvp.key() << " = " << kvp.value() << std::endl;
    } 

    if (result.count("help")) {
        printf("    Usage:\n\
        ./traj_api.exe --[traj|sector] (id) (start_ts) (end_ts)\n\
            Mode flag (--traj or --sector) is required, others are optional.\n\
            All arguments are expected to be in their exact positions.\n\
        Defaults:\n\
            ID: 00000000000000000000000000000000 for trajectory, A for sector\n\
            start_ts: 1699635515 (start of simulation)\n\
            end_ts:   1699671515 (end of simulation)\n\
        Examples:\n\
            ./traj_api --traj                                   // 10-hour trajectory of object 00000000000000000000000000000000\n\
            ./traj_api --traj 000000000000000000000000000001f3  // 10-hour trajectory of object 000000000000000000000000000001f3\n\
            ./traj_api --sector                                 // 10-hour activity of sector A\n\
            ./traj_api --sector B                               // 10-hour activity of sector B\n\
            ./traj_api --sector C 1699640000                    // activity of sector C from t==1699640000 to end of simulation\n\
            ./traj_api --sector D 1699640000 1699650000         // activity of sector D from t==1699640000 to t==1699650000\n");
        return EXIT_FAILURE;    // failure to make a query
    }
    
    string input_log_filename = "simulation_log.txt";
    input_log.open(input_log_filename);
    if (!input_log.is_open()) {
        cerr << "Failed to open input file." << endl;
        return EXIT_FAILURE;
    }

    output_log.open("response.json", ios::out);
    if (!output_log.is_open()) {
        cerr << "Failed to open response.json for writing, aborting." << endl;
        return EXIT_FAILURE;
    }
    cout << "Writing to response.json" << endl;

    if ( ( result.count("traj") &&  result.count("sector"))
    ||   (!result.count("traj") && !result.count("sector")) ) {
        cerr << "Exactly one of either --traj or --sector must be passed as argument." << endl;
        output_log << "{\"Error\": \"Exactly one of either --traj or --sector must be passed as argument.\"}" << endl;
        return EXIT_FAILURE;
    } else if (result.count("traj")) {
        conf.call = API_CALL_TRAJ;
    } else if (result.count("sector")) {
        conf.call = API_CALL_SECTOR;
    }

    if (result.count("id")) {
        if        (conf.call == API_CALL_TRAJ) {
            conf.object_id = result["id"].as<string>();
            // todo check /[0-9a-z]{32}/gmU conformity?
            // right now conformity guaranteed by object constructor

        } else if (conf.call == API_CALL_SECTOR) {
            conf.sector_id = result["id"].as<string>()[0];
            if (conf.sector_id < 'A' || conf.sector_id > 'D') {
                cerr << "Invalid sector given - must be A, B, C, or D." << endl;
                output_log << "{\"Error\": \"Invalid sector given - must be A, B, C, or D.\"}" << endl;
                return EXIT_FAILURE;
            }
        }
    }
    
    if (result.count("start_ts")) {
        conf.start_ts = result["start_ts"].as<uint32_t>();
    }
    if (result.count("end_ts")) {
        conf.end_ts   = result["end_ts"].as<uint32_t>();
    }

    //printf("%s, \n%u, \n%u\n", conf.object_id.c_str(), conf.start_ts, conf.end_ts);

    return EXIT_SUCCESS;
}

int main(int argc, char* argv[]) {
    auto start = chrono::high_resolution_clock::now();

    if (setup(argc, argv) != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }
    output_log << "[";
    // --------------
    
    switch (conf.call) {
        case API_CALL_TRAJ: {
            api_object_traj();
            break;
        } case API_CALL_SECTOR: {
            api_sector_history();
            break;
        }
        default:
            printf("how did you get here? \n");
    }

    // -----------------------------
    input_log.close();
    output_log << endl << "]" << endl;
    output_log.close();

    auto end = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(end - start);
    cout << "Time taken: " << duration.count() << " ms" << endl;

    return 0;
}
