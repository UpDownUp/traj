#include <iomanip>
#include <iostream>
#include <random>
#include <chrono>
#include <fstream>
using namespace std;

#include "cxxopts.hpp"  // cxxopts  3.1.1   MIT (https://github.com/jarro2783/cxxopts)

struct {
    uint8_t verbosity = 0;
    bool logging = false;
    char log_filename[64];

    uint16_t n_objects = 500;
    double simulation_dt = 150;
} conf;

#ifndef _Pi
    #define _Pi 3.141592653589793
#endif

// Hardcoded simulation start/end times
const time_t start_time = 1699635515; // timestamp of the moment when line was written
const time_t end_time   = start_time + (60 * 60 * 10); // + 10 hours (* 60 mins (* 60 secs))

ofstream log_stream;

class Drone {
    typedef struct {
        // Positions are in meters, [0 .. 1 000 000)
        double x;
        double y;
    } pos_t;

    private:
        // - immutable
        char object_id[33];     // /[0-9a-z]{32}/gmU + null terminator
        uint8_t payload[100];

        pos_t orig;
        pos_t wayp;
        pos_t dest;

        double speed;  // m/s
        time_t cts;    // Creation timestamp
        time_t ets;    // Calculated expiration timestamp == cts + (length / speed)

        // - updated in move_until()
        pos_t  actual;  // actual position
        char   sector;  // == 'A' + (actual.x >= 500000) + (2 * (actual.y >= 500000))
        double heading; // heading relative to x-axis; [0 .. 2pi]
        double eta;     // == (1 - p) * (ets - cts)

        // -- Class functions
        // Converts (absolute) time into percentage of curve
        double t_to_p(double a_t) {
            double p = 0;
            if (a_t > cts && a_t < ets) {
                p = (a_t - (double) cts) / (ets - cts);
            } else if (a_t >= ets) {
                p = 1;
            }
            return p;
        }

        // Calculates position as a function of the percentage of the curve (and also orig, wayp and dest points).
        pos_t get_pos_p(double p) {
            if      (p == 0) return orig;
            else if (p == 1) return dest;

            // orig - waypoint linear interpolation
            double l1x   = (1 - p) * orig.x + (p * wayp.x);
            double l1y   = (1 - p) * orig.y + (p * wayp.y);
            // waypoint - destination linear interpolation
            double l2x   = (1 - p) * wayp.x + (p * dest.x);
            double l2y   = (1 - p) * wayp.y + (p * dest.y);
            // quadratic interpolation
            pos_t posp;
            posp.x = (1 - p) * l1x + (p * l2x);
            posp.y = (1 - p) * l1y + (p * l2y);
            return posp;
        }

        // Returns heading angle wrt x-axis by generating a direction vector from pos(p) and pos(p - dt).
        double get_angle_p(double p, double dt) {
            if (p == 0 || p == 1) {
                return 0;
            }
            pos_t front = get_pos_p(p);
            pos_t back  = get_pos_p(p - dt);

            double angle = atan2(front.y - back.y, front.x - back.x);
            if (angle < 0) {
                // atan2() returns angle in [-pi .. pi] range, spec requires [0 .. 2pi]
                angle += 2 * _Pi;
            }
            return angle;
        }

        // dp: percentage of curve moved on each iteration (0..1)
        // Runs 1/dp cycles.
        // Returns estimated length of bezier curve in meters
        double estimate_length(double dp) {
            pos_t cur = orig;
            pos_t last = cur;
            double totaldist = 0;
            for (double p = dp; p <= 1.0001; p += dp) {
                pos_t posc = get_pos_p(p);
                double dx = abs(posc.x - last.x);
                double dy = abs(posc.y - last.y);
                totaldist += sqrt(pow(dx, 2) + pow(dy, 2));

                //printf("p = %f\t x: %f\t y: %f\t dx: %f\t dy: %f\n", p, myself.actual.x, myself.actual.y, dx, dy);
                last.x = posc.x;
                last.y = posc.y;
            }
            return totaldist;
        }

    public:
        // -- Constructor
        Drone(const uint32_t serial[4]) {
            // -- First, assign object ID string from given serial number.
            // Serial number generation is left to the caller.
            sprintf(object_id, "%.8x%.8x%.8x%.8x",
                    serial[0], serial[1], serial[2], serial[3]);
            
            // -- Random generations
            random_device seed;
            minstd_rand prng(seed()); // cheapest RNG, use mt19937 if more randomness required
            srand(seed());  // even cheaper RNG for one-liners

            // Generate payload
            uniform_int_distribution<> rng_hex(0x00, 0xFF);
            for (uint8_t i = 0; i < 100; i++) {
                payload[i] = rng_hex(prng);
            }

            speed  = (((double) rand() / RAND_MAX) * 70) + 10;   //m/s
            // Generate origin, waypoint and destination points
            uniform_real_distribution<> rng_million(0.0, 1000000.0);
            orig.x = rng_million(prng);
            orig.y = rng_million(prng);
            // Reroll waypoint and destination until spec is met
            double waypdist;
            do {
                wayp.x = rng_million(prng);
                wayp.y = rng_million(prng);
                waypdist = sqrt(pow(wayp.x - orig.x, 2) + pow(wayp.y - orig.y, 2));
            } while (waypdist < 100000 || waypdist > 150000);
            double destdist;
            do {
                dest.x = rng_million(prng);
                dest.y = rng_million(prng);
                destdist = sqrt(pow(dest.x - orig.x, 2) + pow(dest.y - orig.y, 2));
            } while (destdist < 150000 || destdist > 400000);

            // Estimate length by dividing curve in 100 linear segments
            double length = estimate_length(0.01);

            // RNG the creation timestamp, calculate arrival/expiration timestamp
            cts = (rand() % (end_time - start_time)) + start_time;
            ets = cts + length/speed;

            if (conf.verbosity >= 1) {
                printf("orig.x %.2f\t orig.y %.2f\n", orig.x, orig.y);
                printf("wayp.x %.2f\t wayp.y %.2f\n", wayp.x, wayp.y);
                printf("dest.x %.2f\t dest.y %.2f\n", dest.x, dest.y);
                printf("length: %.2lf m\t speed: %.2lf m/s\n", length, speed);
                printf("cts: %.ld\t ets: %.ld\t travel time: %ld s\n", cts, ets, ets - cts);
            }

            if (conf.logging) {
                // Convert payload to hex string to minimize registration message size
                char payload_str[201] = {0};    // null terminator included
                for (uint16_t byte = 0; byte < 100; byte++) {
                    snprintf(payload_str + (byte * 2), 3, "%02X", payload[byte]);
                }
                // printf("%s\n", payload_str);

                char registration_cstr[512];
                sprintf(registration_cstr, "{\"cts\":%ld,\"dest\":[%.3f,%.3f],\"ets\":%ld,\"id\":\"%s\",\"orig\":[%.3f,%.3f],\"payload\":\"%s\",\"speed\":%.3f,\"wayp\":[%.3f,%.3f]}",
                                                                cts,        dest.x, dest.y,     ets,         object_id,     orig.x, orig.y,         payload_str,        speed,       wayp.x, wayp.y);
                // cout  << registration_msg << endl;
                log_stream << registration_cstr << endl;
            }
        }

        // -- The only legal way to move a drone
        string move_until(double t) {
            double p = t_to_p(t);
            actual   = get_pos_p(p);
            heading  = get_angle_p(p, 0.15);
            eta      = (1 - p) * (ets - cts);
            sector   = 'A' + (actual.x >= 500000) + (2 * (actual.y >= 500000));

            if (conf.verbosity >= 2) {
                printf("t = %.2f\t id = %s, p = %.3f\t x: %.2f\t y = %.2f\t f = %.2lf\t eta = %.2f\t %c\n",
                                t,   object_id + 16, p,        actual.x, actual.y, heading * (180/_Pi), eta, sector);
            }

            string update_str = "";
            if (conf.logging) {
                char update_cstr[256];
                // Doing this with json.hpp multiplies runtime by about 10x.
                sprintf(update_cstr, "{\"eta\":%.2f,\"heading\":%.3f,\"id\":\"%s\",\"pos\":[%.3f,%.3f],\"sector\":\"%c\",\"t\":%.3f}",
                                                             eta,        heading,    object_id,      actual.x, actual.y,        sector,        t);
                //cout  << update_msg << endl;
                update_str = update_cstr;
            }
            return update_str;
        }
};

vector<Drone> generate_n_drones(unsigned int n) {
    vector<Drone> drones_vec;
    for (uint32_t i = 0; i < n; i++) {
        uint32_t sn = i;
        uint32_t serial[4] = {0, 0, 0, sn};
        Drone droney(serial);
        drones_vec.push_back(droney);
    }
    return drones_vec;
}

// aka pass_time()
void run_simulation(vector<Drone> drones, double dt) {
    for (double t = (double) start_time; t < (double) end_time; t += dt) {
        // Buffer all drone update messages in current timestamp
        // into a single string (~60 KB for n == 500) for faster write
        stringstream thedump;
        for (Drone drone_i : drones) {
            string update = drone_i.move_until(t);
            if (conf.logging) thedump << update << endl;
        }
        if (conf.logging) log_stream << thedump.str();
    }
}

uint8_t set_conf(int argc, char* argv[]) {
    cxxopts::Options options("traj.cpp", "Simulate trajectory of flying objects");

    options.add_options()
        //v  -- Default: minimal stdout output; -v: creation logs; -vv: creation and trajectory logs
        ("v,verbosity", "Verbosity")
        //l  -- Default false; -l: logs JSON-style (but not JSON-structured) creation and trajectory messages
        ("l,log", "Logging")
        //n  -- Default 500, [0 .. 1000]
        ("n,n_objects", "Number of objects simulated", cxxopts::value<uint32_t>())
        //dt -- Default 150 (0.15 in spec), (0 .. 18000) 
        ("d,dt", "Time interval", cxxopts::value<double>())
        ("h,help", "Show info")
    ;
    cxxopts::ParseResult result;
    try {
        result = options.parse(argc, argv);
    } catch(cxxopts::exceptions::exception e) {
        cout << "Exception caught during argument parsing: " << e.what() << endl;
        return EXIT_FAILURE;
    }

    std::cout << "Parsed options:" << std::endl;
    for (const auto& kvp : result.arguments()) {
        std::cout << kvp.key() << " = " << kvp.value() << std::endl;
    }
    
    if (result.count("help")) {
        printf("    Usage:\n\
        ./traj.exe (-v(v)) (-l) (-n $n_objects) (-d $dt)\n\
            All arguments optional. By default, generate a simulation of 500 objects,\n\
            with a time interval of 150 seconds between updates,\n\
            logs nothing to stdout nor to simulation_log.txt.\n\
        Examples:\n\
            ./traj -l  -n 500 -d 0.15  // Generates simulation to spec, logs to simulation_log.txt\n\
            ./traj -v  -n 5            // Create simulation of 5 objects, print only registration messages to stdout\n\
            ./traj -vv -n 5   -d 1500  // Create simulation of 5 objects with 1500 seconds' interval,\n\
                                          log registration and update messages to stdout\n\
            ./traj -l  -n 50  -d 150   // you get the idea, logs everything, prints only diagnostics to stdout\n");
        return EXIT_FAILURE;    // failure to make a query
    }

    conf.verbosity      = result.count("v");
    conf.logging        = (bool) result.count("l");

    // max objects allowed is 1000 (arbitrary), inclusive
    if (result.count("n_objects") && result["n_objects"].as<uint32_t>() <= 1000)
        conf.n_objects  = result["n_objects"].as<uint32_t>();
    // dt can be any positive number under 18000 seconds (5 hours), exclusive
    if (result.count("dt") && result["dt"].as<double>() > 0 && result["dt"].as<double>() < 18000)
        conf.simulation_dt = result["dt"].as<double>();
    //sprintf(conf.log_filename, "%d-%.3f-%ld.txt", conf.n_objects, conf.simulation_dt, time(NULL));
    sprintf(conf.log_filename, "simulation_log.txt");
    
    if (conf.logging) {
        log_stream.open(conf.log_filename, ios::out);
        if (!log_stream.is_open()) {
            cout << "Failed to open " << conf.log_filename << " for writing, aborting." << endl;
            return EXIT_FAILURE;
        }
        cout << "Writing to " << conf.log_filename << endl;

        // Write config to first line
        char conf_cstr[64];
        sprintf(conf_cstr, "{\"n\": %d, \"dt\": %.3f}", conf.n_objects, conf.simulation_dt);
        // cout << conf_msg.dump() << endl;
        log_stream << conf_cstr << endl;
    }

    return EXIT_SUCCESS;
}

int main(int argc, char* argv[]) {
    if (set_conf(argc, argv) != EXIT_SUCCESS) {
        return EXIT_FAILURE;
    }

    auto start = chrono::high_resolution_clock::now();

    run_simulation(generate_n_drones(conf.n_objects), conf.simulation_dt);

    log_stream.close();

    auto end = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(end - start);
    cout << "Time taken: " << duration.count() << " ms" << endl;

    return EXIT_SUCCESS;
}
