from flask import Flask, jsonify, request, send_file
import subprocess
import os

app = Flask(__name__)

@app.route('/api/traj', methods=['POST'])
def traj_api_handler():
    try:
        data = request.get_json()

        mode     = data.get('mode') # required
        sid      = data.get('id')   # required   # todo check regex conformity?
        start_ts = data.get('start_ts', "0")
        end_ts   = data.get('end_ts',   "0xFFFFFFFF")
        # If arguments not valid, server will return error by this point.        

        subprocess.run(["./traj_api", "--" + mode, sid, start_ts, end_ts])
        # Check if response exists
        if os.path.exists('response.json'):
            # Return the file as a response
            return send_file('response.json', as_attachment=True)
        else:
            return jsonify({'message': 'Error', 'error_details': 'response.json not found'})

    except Exception as e:
        return jsonify({'message': 'Error', 'error_details': str(e)})

if __name__ == '__main__':
    app.run(debug=True)
